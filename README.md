# Traeger Mobile Code Challenge


### The Challenge
Create a single page site that lists the NASA Astronomy Picture of the Day for the past 7 days. Tapping on the list should display the image and description. It is up to you how you do this. Be creative and have fun!
 
To get the list, you will need to go to https://api.nasa.gov. Sign up for a free API key, then use the API https://api.nasa.gov/planetary/apod.

### What we are looking for
Solid fundamentals in coding and code organization. Not looking for anything fancy, nor anything perfect. Just simple, clean code. We don't expect you to spend more than 2-4 hour or so on this. Just get the MVP working and be prepared to discuss what you wrote and what you would do next.

##### iOS Note
On iOS, we want to see your SwiftUI skills along with RxSwift and Combine. You are open to using an app architecture that you prefer and we can discuss your choice.

##### Android Note
On Android, we would love to see your skills with MVP architecture and your use of reusable views or even Jetpack Compose.


### Deadline
You have 24 hours from the time you receive the email to complete the challenge.


### Submitting
Upload your project to gitlab and add `traeger.interview` as a collaborator. If you use a different repo, contact us with the details and we will make arrangements.
